<?php

// phpcs:ignoreFile

namespace App\Command\Generator;

use Symfony\Component\Console\Command\Command;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class ControllerGenerator extends Command
{
    use Generator;

    protected static $defaultName = 'app:generate:controller';

    public function startGenerator()
    {
        $path = $this->projetPath.'src/UI/Api/Controller/' . ucfirst($this->apiName) . 'Controller.php';
        if ('' != $this->folder) {
            if (!file_exists($this->projetPath.'src/UI/Api/Controller/' . $this->folder)) {
                mkdir($this->projetPath.'src/UI/Api/Controller/' . $this->folder);
            }
            $path = $this->projetPath.'src/UI/Api/Controller/' . $this->folder . '/' . ucfirst($this->apiName) . 'Controller.php';
        }

        $this->controllerGenerate($path);
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function controllerGenerate(string $path)
    {

        $controller = fopen($path, 'c+b');
        ftruncate($controller, 0);
        fwrite($controller, '<?php

namespace App\UI\Api\Controller');
        if ('' != $this->folder) {
            fwrite($controller, '\\' . str_replace('/', '\\', $this->folder));
        }
        fwrite($controller, ';

use App\Application\Exception\ValidationException;
use App\Application\Manager\SecurityManager;
use App\Application\Response\HTTP\ClientError\ForbiddenJsonResponse;
use App\Application\Response\HTTP\ClientError\NotFoundJsonResponse;
use App\Application\Response\HTTP\Success\CreatedJsonResponse;
use App\Application\Response\HTTP\Success\NoContentJsonResponse;
use App\Application\Response\HTTP\Success\OKJsonResponse;
use App\Application\Response\IndexJsonResponse;
use App\Application\Service\\');
        if ('' != $this->folder) {
            fwrite($controller, str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($controller, ucfirst($this->apiName) . 'Service;
use App\Domain\Model\Role;
use App\Domain\ValueObject\UuidVO;
use App\UI\Api\Input\IndexInput;
use App\UI\Api\Input\\');
        if ('' != $this->folder) {
            fwrite($controller, str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($controller, ucfirst($this->apiName) . '\Input;
use App\UI\Api\Resource\\');
        if ('' != $this->folder) {
            fwrite($controller, str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($controller, ucfirst($this->apiName) . ' as ' . ucfirst($this->apiName) . 'Resource;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Ramsey\Uuid\Uuid;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class ' . ucfirst($this->apiName) . 'Controller extends AbstractController
{

    /**
     * List ' . $this->apiNameS . '.
     *
     * @Route(
     *     "/api/' . $this->apiNameS . '",
     *      methods={"GET"},
     *      name="' . $this->convertToPointCase($this->apiName) . '.index",
     *     condition="request.headers.get(\'Accept\') matches \'%app.version%\'",
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="List of ' . $this->convertToEspaceCase($this->apiNameS) . '.",
     *     @Model(type=' . ucfirst($this->apiName) . 'Resource::class)
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You are not allowed to access ' . $this->convertToEspaceCase($this->apiNameS) . '."
     * )
     * @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     type="integer",
     *     description="Page",
     *     default=1
     * )
     * @SWG\Parameter(
     *     name="per_page",
     *     in="query",
     *     type="integer",
     *     description="Number of elements per page",
     *     default=20
     * )
     * @SWG\Tag(name="' . ucfirst($this->apiNameS) . '")
     * @Security(name="oauth2")
     *
     * @SWG\Tag(name="' . ucfirst($this->apiNameS) . '")
     * @Security(name="oauth2")
     *
     * @param IndexInput    $input
     * @param ' . ucfirst($this->apiName) . 'Service $' . $this->apiName . 'Service
     * @param SecurityManager  $securityManager
     *
     * @return JsonResponse
     */
    public function index(
        IndexInput $input,
        ' . ucfirst($this->apiName) . 'Service $' . $this->apiName . 'Service,
        SecurityManager $securityManager
    ): JsonResponse {
        if (!$securityManager->canActOn(Role::' . $this->role . ')) {
            return new ForbiddenJsonResponse(\'You are not allowed to access ' . $this->convertToEspaceCase($this->apiNameS) . '.\');
        }

        $' . $this->apiNameS . ' = $' . $this->apiName . 'Service->listPaginated(
            $input->getQ(),
            $input->getPage(),
            $input->getPerPage()
        );

        $totalResults = $' . $this->apiName . 'Service->count($input->getQ());

        return new IndexJsonResponse(
            array_map(
                static function ($' . $this->apiName . ') {
                    return (new ' . ucfirst($this->apiName) . 'Resource($' . $this->apiName . '))->data();
                },
                $' . $this->apiNameS . '
            ),
            $totalResults,
            $input->getPerPage()
        );
    }

    /**
     * Get a ' . ucfirst($this->apiName) . '.
     *
     * @Route(
     *     "/api/' . $this->apiNameS . '/{' . $this->apiName . 'Id}",
     *      methods={"GET"},
     *      name="' . $this->convertToPointCase($this->apiName) . '.show",
     *     condition="request.headers.get(\'Accept\') matches \'%app.version%\'",
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns information about an ' . $this->convertToEspaceCase($this->apiName) . '.",
     *     @Model(type=' . ucfirst($this->apiName) . 'Resource::class)
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You are not allowed to access this ' . $this->convertToEspaceCase($this->apiName) . '."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="' . ucfirst($this->apiName) . ' not found."
     * )
     * @SWG\Parameter(
     *     name="' . $this->apiName . 'Id",
     *     in="path",
     *     type="string",
     *     description="' . $this->apiName . ' ID"
     * )
     * @SWG\Tag(name="' . ucfirst($this->apiNameS) . '")
     * @Security(name="oauth2")
     *
     * @param string      $' . $this->apiName . 'Id
     * @param ' . ucfirst($this->apiName) . 'Service $' . $this->apiName . 'Service
     * @param SecurityManager  $securityManager
     *
     * @return JsonResponse
     */
    public function show(
        string $' . $this->apiName . 'Id,
        ' . ucfirst($this->apiName) . 'Service $' . $this->apiName . 'Service,
        SecurityManager $securityManager
    ): JsonResponse {
        if (!$securityManager->canActOn(Role::' . $this->role . ')) {
            return new ForbiddenJsonResponse(\'You are not allowed to access this ' . $this->convertToEspaceCase($this->apiName) . '.\');
        }

        if (Uuid::isValid($' . $this->apiName . 'Id)) {
            $' . $this->apiName . ' = $' . $this->apiName . 'Service->find(UuidVO::fromString($' . $this->apiName . 'Id));
        }

        if (!isset($' . $this->apiName . ')) {
            return new NotFoundJsonResponse(\'' . ucfirst($this->convertToEspaceCase($this->apiName)) . ' not found.\');
        }

        return new OkJsonResponse(
            (new ' . ucfirst($this->apiName) . 'Resource($' . $this->apiName . '))->data()
        );
    }

    /**
     * Store a ' . ucfirst($this->apiName) . '.
     *
     * @Route(
     *     "/api/' . $this->apiNameS . '",
     *      methods={"POST"},
     *      name="' . $this->convertToPointCase($this->apiName) . '.store",
     *     condition="request.headers.get(\'Accept\') matches \'%app.version%\'",
     * )
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created ' . $this->apiName . '.",
     *     @Model(type=' . ucfirst($this->apiName) . 'Resource::class)
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You are not allowed to store ' . $this->convertToEspaceCase($this->apiName) . '."
     * )
     * @SWG\Response(
     *     response=422,
     *     description="' . ucfirst($this->convertToEspaceCase($this->apiName)) . ' already exists.",
     * )
     * @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     @Model(type=Input::class)
     * )
     * @SWG\Tag(name="' . ucfirst($this->apiNameS) . '")
     * @Security(name="oauth2")
     * 
     * @param ' . ucfirst($this->apiName) . 'Service $' . $this->apiName . 'Service
     * @param SecurityManager  $securityManager
     *
     * @return JsonResponse
     */
    public function store(
        Input $input,
        ' . ucfirst($this->apiName) . 'Service $' . $this->apiName . 'Service,
        SecurityManager $securityManager
    ): JsonResponse {
        if (!$securityManager->canActOn(Role::' . $this->role . ')) {
            return new ForbiddenJsonResponse(\'You are not allowed to store ' . $this->convertToEspaceCase($this->apiNameS) . '.\');
        }

        try {
            return new CreatedJsonResponse(
                (new ' . ucfirst($this->apiName) . 'Resource($' . $this->apiName . 'Service->create' . $this->apiName . '($input)))->data()
            );
        } catch (UniqueConstraintViolationException $e) {
            $constraintViolations = new ConstraintViolationList([
                new ConstraintViolation(
                    \'' . ucfirst($this->apiName) . ' already exists.\',
                    null,
                    [],
                    \'\',
                    \'' . $this->properties[1][0] . '\',
');
        if ($this->properties[1]) {
            $property = $this->properties[1];

            if ('bool' != $property[1]) {
                fwrite(
                    $controller,
                    '                    $input->get' . ucfirst($property[0]) . '(),
'
                );
            }
            if ('bool' == $property[1]) {
                fwrite(
                    $controller,
                    '              $input->is' . ucfirst($property[0]) . '(),
'
                );
            }
        }

        if (!$this->properties[1]) {
            fwrite($controller, '              \'\' 
');
        }

        fwrite($controller, '
                ),
            ]);

            throw new ValidationException($constraintViolations);
        }
    }

    /**
     * Update a ' . $this->apiName . '.
     *
     * @Route(
     *     "/api/' . $this->apiNameS . '/{' . $this->apiName . 'Id}",
     *      methods={"PUT"},
     *      name="' . $this->convertToPointCase($this->apiName) . '.update",
     *     condition="request.headers.get(\'Accept\') matches \'%app.version%\'",
     * )
     * @SWG\Parameter(
     *     name="' . $this->apiName . 'Id",
     *     in="path",
     *     type="string",
     *     description="' . ucfirst($this->apiName) . ' ID"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns the new ' . $this->convertToEspaceCase($this->apiNameS) . ' informations."
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You are not allowed to update this ' . $this->convertToEspaceCase($this->apiName) . '."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="' . ucfirst($this->convertToEspaceCase($this->apiName)) . ' not found"
     * )
     * @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     @Model(type=Input::class)
     * )
     * @SWG\Tag(name="' . ucfirst($this->apiNameS) . '")
     * @Security(name="oauth2")
     * 
     * @param string      $' . $this->apiName . 'Id
     * @param ' . ucfirst($this->apiName) . 'Service $' . $this->apiName . 'Service
     * @param SecurityManager  $securityManager
     *
     * @return JsonResponse
     */
    public function update(
        Input $input,
        string $' . $this->apiName . 'Id,
        ' . ucfirst($this->apiName) . 'Service $' . $this->apiName . 'Service,
        SecurityManager $securityManager
    ): JsonResponse {
        if (!$securityManager->canActOn(Role::' . $this->role . ')) {
            return new ForbiddenJsonResponse(\'You are not allowed to update this ' . $this->convertToEspaceCase($this->apiNameS) . '.\');
        }

        if (Uuid::isValid($' . $this->apiName . 'Id)) {
            $' . $this->apiName . ' = $' . $this->apiName . 'Service->find(UuidVO::fromString($' . $this->apiName . 'Id));
        }

        if (!isset($' . $this->apiName . ')) {
            return new NotFoundJsonResponse(\'' . $this->convertToEspaceCase($this->apiName) . ' not found.\');
        }

        try {
            $' . $this->apiName . 'Service->update' . ucfirst($this->apiName) . '($' . $this->apiName . ', $input);

            return new OKJsonResponse((new ' . ucfirst($this->apiName) . 'Resource($' . $this->apiName . '))->data());
        } catch (UniqueConstraintViolationException $e) {
            $constraintViolations = new ConstraintViolationList([
                new ConstraintViolation(
                    \'' . ucfirst($this->apiName) . ' already exists.\',
                    null,
                    [],
                    \'\',
                    \'' . $this->properties[1][0] . '\',
');
        if ($this->properties[1]) {
            $property = $this->properties[1];

            if ('bool' != $property[1]) {
                fwrite(
                    $controller,
                    '                    $input->get' . ucfirst($property[0]) . '(),
'
                );
            }
            if ('bool' == $property[1]) {
                fwrite(
                    $controller,
                    '              $input->is' . ucfirst($property[0]) . '(),
'
                );
            }
        }
        if ($this->properties[1]) {
            fwrite($controller, '              \'\' 
');
        }

        fwrite($controller, '
                ),
            ]);

            throw new ValidationException($constraintViolations);
        }
    }

    /**
     * Delete a ' . $this->apiName . '.
     *
     * @Route(
     *     "/api/' . $this->apiNameS . '/{' . $this->apiName . 'Id}",
     *      methods={"DELETE"},
     *      name="' . $this->convertToPointCase($this->apiName) . '.delete",
     * )
     * @SWG\Response(
     *     response=204,
     *     @Model(type=' . ucfirst($this->apiName) . 'Resource::class),
     *     description="Return code if the resource is successfully removed"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You are not allowed to delete this ' . $this->convertToEspaceCase($this->apiNameS) . '."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="' . ucfirst($this->convertToEspaceCase($this->apiNameS)) . ' not found."
     * )
     * @SWG\Parameter(
     *     name="' . $this->apiName . 'Id",
     *     in="path",
     *     type="string",
     *     description="' . ucfirst($this->apiName) . ' ID"
     * )
     * @SWG\Tag(name="' . ucfirst($this->apiNameS) . '")
     * @Security(name="oauth2")
     * 
     * @param string      $' . $this->apiName . 'Id
     * @param ' . ucfirst($this->apiName) . 'Service $' . $this->apiName . 'Service
     * @param SecurityManager  $securityManager
     *
     * @return JsonResponse
     */
    public function delete(
        string $' . $this->apiName . 'Id,
        ' . ucfirst($this->apiName) . 'Service $' . $this->apiName . 'Service,
        SecurityManager $securityManager
    ): JsonResponse {
        if (!$securityManager->canActOn(Role::' . $this->role . ')) {
            return new ForbiddenJsonResponse(\'You are not allowed to delete this ' . $this->convertToEspaceCase($this->apiNameS) . '.\');
        }

        if (Uuid::isValid($' . $this->apiName . 'Id)) {
            $' . $this->apiName . ' = $' . $this->apiName . 'Service->find(UuidVO::fromString($' . $this->apiName . 'Id));
        }

        if (!isset($' . $this->apiName . ')) {
            return new NotFoundJsonResponse(\'' . ucfirst($this->convertToEspaceCase($this->apiNameS)) . ' not found.\');
        }

        $' . $this->apiName . 'Service->delete' . ucfirst($this->apiName) . '($' . $this->apiName . ');

        return new NoContentJsonResponse();
    }
}
');
        fclose($controller);
    }
}
