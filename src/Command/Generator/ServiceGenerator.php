<?php

// phpcs:ignoreFile

namespace App\Command\Generator;

use Symfony\Component\Console\Command\Command;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class ServiceGenerator extends Command
{
    use Generator;

    protected static $defaultName = 'app:generate:service';

    public function startGenerator()
    {
        $path = $this->projetPath.'src/Application/Service/' . ucfirst($this->apiName) . 'Service.php';
        if ('' != $this->folder) {
            if (!file_exists($this->projetPath.'src/Application/Service/' . $this->folder)) {
                mkdir($this->projetPath.'src/Application/Service/' . $this->folder);
            }
            $path = $this->projetPath.'src/Application/Service/' . $this->folder . '/' . ucfirst($this->apiName) . 'Service.php';
        }
        $this->serviceGenerate($path);
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function serviceGenerate(string $path)
    {
        $service = fopen($path, 'c+b');
        ftruncate($service, 0);
        fwrite($service, '<?php
namespace App\Application\Service');
        if ('' != $this->folder) {
            fwrite($service, '\\' . str_replace('/', '\\', $this->folder));
        }
        fwrite($service, ';

use App\Domain\Model\\');
        if ('' != $this->folder) {
            fwrite($service, '\\' . str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($service, '' . ucfirst($this->apiName) . ';
use App\Domain\Persister\\');
        if ('' != $this->folder) {
            fwrite($service, '\\' . str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($service, '' . ucfirst($this->apiName) . 'PersisterInterface;
use App\Domain\Repository\\');
        if ('' != $this->folder) {
            fwrite($service, '\\' . str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($service, '' . ucfirst($this->apiName) . 'RepositoryInterface;
use App\Domain\ValueObject\UuidVO;
use App\UI\Api\Input\\');
        if ('' != $this->folder) {
            fwrite($service, '\\' . str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($service, '' . ucfirst($this->apiName) . '\Input;

/**
 * Class ' . ucfirst($this->apiName) . 'Service.
*/
class ' . ucfirst($this->apiName) . 'Service
{
    /**
     * @var ' . ucfirst($this->apiName) . 'PersisterInterface
    */
    private $' . $this->apiName . 'Persister;

    /**
     * @var ' . ucfirst($this->apiName) . 'RepositoryInterface
    */
    private $' . $this->apiName . 'Repository;

    /**
     * ' . $this->apiName . 'Service constructor.
    *
    * @param ' . ucfirst($this->apiName) . 'RepositoryInterface $' . $this->apiName . 'Repository
    * @param ' . ucfirst($this->apiName) . 'PersisterInterface  $' . $this->apiName . 'Persister
    */
    public function __construct(
        ' . ucfirst($this->apiName) . 'RepositoryInterface $' . $this->apiName . 'Repository,
        ' . ucfirst($this->apiName) . 'PersisterInterface $' . $this->apiName . 'Persister
    ) {
        $this->' . $this->apiName . 'Repository = $' . $this->apiName . 'Repository;
        $this->' . $this->apiName . 'Persister = $' . $this->apiName . 'Persister;
    }

    /**
     * @param Input $input
    *
    * @throws \Exception
    *
    * @return ' . $this->apiName . '
    */
    public function create' . $this->apiName . '(Input $input): ' . ucfirst($this->apiName) . '
    {
        $' . $this->apiName . ' = ' . ucfirst($this->apiName) . '::create(
');
        $count = 0;
        foreach ($this->properties as $property) {
            ++$count;
            if (filter_var($property[9], FILTER_VALIDATE_BOOLEAN)) {
                if ('uuid' != $property[0]) {
                    fwrite($service, '            ');
                    if ('bool' != $property[1]) {
                        fwrite($service, '$input->get' . ucfirst($property[0]) . '()');
                    }
                    if ('bool' == $property[1]) {
                        fwrite($service, '$input->is' . ucfirst($property[0]) . '()');
                    }
                    if ($count < $this->sizeOfProperties) {
                        fwrite($service, ',
');
                    }
                }
            }

            if (!filter_var($property[9], FILTER_VALIDATE_BOOLEAN) && 'uuid' != $property[0]) {
                fwrite($service, '            null');
                if ($count < $this->sizeOfProperties) {
                    fwrite($service, ',
');
                }
            }
        }
        fwrite($service, '
        );
        $this->' . $this->apiName . 'Persister->startTransaction();

        try {
            $this->' . $this->apiName . 'Persister->store($' . $this->apiName . ');

            $this->' . $this->apiName . 'Persister->commit();
        } catch (\Exception $e) {
            $this->' . $this->apiName . 'Persister->rollback();
            throw $e;
        }
        return $' . $this->apiName . ';
    }

    /**
     * @param ' . ucfirst($this->apiName) . '  $' . $this->apiName . '
    * @param Input $input
    *
    * @return ' . $this->apiName . '
    */
    public function update' . ucfirst($this->apiName) . '(' . $this->apiName . ' $' . $this->apiName . ', Input $input): ' . $this->apiName . '
    {
        $' . $this->apiName . '->update(
');
        $count = 0;
        foreach ($this->properties as $property) {
            if (filter_var($property[8], FILTER_VALIDATE_BOOLEAN) && filter_var($property[9], FILTER_VALIDATE_BOOLEAN)) {
                ++$count;
                if (!filter_var($property[9], FILTER_VALIDATE_BOOLEAN) && 'uuid' != $property[0]) {
                    fwrite($service, '            null');
                    if ($count < $this->numberPropertyUpdate) {
                        fwrite($service, ',
    ');
                    }
                }
                fwrite($service, '        ');
                if ('bool' != $property[1] && filter_var($property[9], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($service, '    $input->get' . ucfirst($property[0]) . '()');
                }
                if ('bool' == $property[1] && filter_var($property[9], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($service, '    $input->is' . ucfirst($property[0]) . '()');
                }
                if ($count < $this->numberPropertyUpdate) {
                    fwrite($service, ',
');
                }
            }
        }
        fwrite($service, '
        );

        $this->' . $this->apiName . 'Persister->startTransaction();

        try {
            $this->' . $this->apiName . 'Persister->update($' . $this->apiName . ');

            $this->' . $this->apiName . 'Persister->commit();
        } catch (\Exception $e) {
            $this->' . $this->apiName . 'Persister->rollback();
            throw $e;
        }

        return $' . $this->apiName . ';
    }

    /**
     * @param ' . ucfirst($this->apiName) . ' $' . $this->apiName . '
    *
    * @return ' . $this->apiName . '
    */
    public function delete' . ucfirst($this->apiName) . '(' . $this->apiName . ' $' . $this->apiName . '): ' . $this->apiName . '
    {
        $this->' . $this->apiName . 'Persister->delete($' . $this->apiName . ');

        return $' . $this->apiName . ';
    }

    /**
     * @param UuidVO $. $this->apiName .Uuid
     *
     * @return ucfirst($this->apiName)|null
     */
    public function find(UuidVO $' . $this->apiName . 'Uuid): ?' . $this->apiName . '
    {
        return $this->' . $this->apiName . 'Repository->find([
            \'uuid\' => $' . $this->apiName . 'Uuid,
        ]);
    }

    /**
     * @param string $q
     * @param int    $page
     * @param int    $perPage
     *
     * @return array
     */
    public function listPaginated(
        string $q,
        int $page = 1,
        int $perPage = 20
    ): array {
        $filters = "" !== $q ? json_decode($q, true) : [];
        return $this->' . $this->apiName . 'Repository->list(
            $filters,
            $page,
            $perPage
        );
    }

    /**
     * @param string $q
     *
     * @return int
     */
    public function count(
        string $q
    ): int {
        $filters = "" !== $q ? json_decode($q, true) : [];
        return $this->' . $this->apiName . 'Repository->count(
            $filters
        );
    }
}
');
        fclose($service);
    }
}
