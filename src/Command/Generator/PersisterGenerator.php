<?php

// phpcs:ignoreFile

namespace App\Command\Generator;

use Symfony\Component\Console\Command\Command;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class PersisterGenerator extends Command
{
    use Generator;

    protected static $defaultName = 'app:generate:persister';

    public function startGenerator()
    {
        $path = $this->projetPath.'src/Domain/Persister/' . ucfirst($this->apiName) . 'PersisterInterface.php';
        if ('' != $this->folder) {
            if (!file_exists($this->projetPath.'src/Domain/Persister/' . $this->folder)) {
                mkdir($this->projetPath.'src/Domain/Persister/' . $this->folder);
            }
            $path = $this->projetPath.'src/Domain/Persister/' . $this->folder . '/' . ucfirst($this->apiName) . 'PersisterInterface.php';
        }

        $this->persisterInterfaceGenerate($path);

        $path = $this->projetPath.'src/Infrastructure/MySQL/Persister/' . ucfirst($this->apiName) . 'Persister.php';
        if ('' != $this->folder) {
            if (!file_exists($this->projetPath.'src/Infrastructure/MySQL/Persister/' . $this->folder)) {
                mkdir($this->projetPath.'src/Infrastructure/MySQL/Persister/' . $this->folder);
            }
            $path = $this->projetPath.'src/Infrastructure/MySQL/Persister/' . $this->folder . '/' . ucfirst($this->apiName) . 'Persister.php';
        }

        $this->persisterGenerate($path);
    }

    private function persisterInterfaceGenerate(string $path)
    {

        $persisterInterface = fopen($path, 'c+b');
        ftruncate($persisterInterface, 0);
        fwrite($persisterInterface, '<?php

namespace App\Domain\Persister');
        if ('' != $this->folder) {
            fwrite($persisterInterface, '\\' . str_replace('/', '\\', $this->folder) . '');
        }
        fwrite($persisterInterface, ';

use App\Domain\Model\\');
        if ('' != $this->folder) {
            fwrite($persisterInterface, '' . str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($persisterInterface, '' . ucfirst($this->apiName) . ';
use App\Domain\Persister\BasePersisterInterface;

interface ' . ucfirst($this->apiName) . 'PersisterInterface extends BasePersisterInterface
{
    /**
     * Store a ' . $this->convertToEspaceCase($this->apiName) . ' in storage.
     *
     * @param ' . ucfirst($this->apiName) . ' $' . $this->apiName . '
     *
     * @return ' . ucfirst($this->apiName) . '
     */
    public function store(' . ucfirst($this->apiName) . ' $' . $this->apiName . '): ' . $this->apiName . ';

    /**
     * Update a ' . $this->convertToEspaceCase($this->apiName) . ' in storage.
     *
     * @param ' . ucfirst($this->apiName) . ' $' . $this->apiName . '
     *
     * @return ' . ucfirst($this->apiName) . '
     */
    public function update(' . ucfirst($this->apiName) . ' $' . $this->apiName . '): ' . ucfirst($this->apiName) . ';

    /**
     * Delete a ' . $this->convertToEspaceCase($this->apiName) . ' in storage.
     *
     * @param ' . ucfirst($this->apiName) . ' $' . $this->apiName . '
     */
    public function delete(' . ucfirst($this->apiName) . ' $' . $this->apiName . ');
}
');
        fclose($persisterInterface);
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function persisterGenerate(string $path)
    {
        $persister = fopen($path, 'c+b');
        ftruncate($persister, 0);
        fwrite($persister, '<?php

namespace App\Infrastructure\MySQL\Persister');
        if ('' != $this->folder) {
            fwrite($persister, '\\' . str_replace('/', '\\', $this->folder));
        }
        fwrite($persister, ';

use App\Domain\Model\\');
        if ('' != $this->folder) {
            fwrite($persister, '' . str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($persister, '' . ucfirst($this->apiName) . ';
use App\Domain\Persister\\');
        if ('' != $this->folder) {
            fwrite($persister, '' . str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($persister, '' . ucfirst($this->apiName) . 'PersisterInterface;
use App\Infrastructure\MySQL\Persister\BasePersister;
use Doctrine\DBAL\Types\Types;
');
        if (50 <= $this->sizeOfProperties) {
            fwrite($persister, ' 
/**
    * @SuppressWarnings(ExcessiveMethodLength)
*/');
        }

        fwrite($persister, '
class ' . ucfirst($this->apiName) . 'Persister extends BasePersister implements ' . ucfirst($this->apiName) . 'PersisterInterface
{
    /**
     * @param ' . ucfirst($this->apiName) . ' $' . $this->apiName . '
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @return ' . ucfirst($this->apiName) . '
     */
    public function store(' . ucfirst($this->apiName) . ' $' . $this->apiName . '): ' . ucfirst($this->apiName) . '
    {
        $this->connection->insert(
            \'' . $this->convertToSnakeCase($this->apiNameS) . '\',
            [
        ');
        $count = 0;
        foreach ($this->properties as $property) {
            ++$count;
            if ('bool' != $property[1]) {
                fwrite($persister, '        \'' . $this->convertToSnakeCase($property[0]) . '\' => $' . $this->apiName . '->get' . ucfirst($property[0]) . '()');
            }

            if ('bool' == $property[1]) {
                fwrite($persister, '        \'' . $this->convertToSnakeCase($property[0]) . '\' => $' . $this->apiName . '->is' . ucfirst($property[0]) . '()');
            }
            if ($count < $this->sizeOfProperties) {
                fwrite($persister, ',
        ');
            }
        }
        fwrite($persister, '
            ],
            [
            ');
        $count = 0;
        foreach ($this->properties as $property) {
            ++$count;
            fwrite($persister, '    \'' . $this->convertToSnakeCase($property[0]) . '\' => Types::' . strtoupper($this->convert($property[1], 3)) . '');
            if ($count < $this->sizeOfProperties) {
                fwrite($persister, ',
            ');
            }
        }
        fwrite($persister, '
            ]
        );

        return $' . $this->apiName . ';
    }

    /**
     * @param ' . ucfirst($this->apiName) . ' $' . $this->apiName . '
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @return ' . ucfirst($this->apiName) . '
     */
    public function update(' . ucfirst($this->apiName) . ' $' . $this->apiName . '): ' . ucfirst($this->apiName) . '
    {
        $this->connection->update(
            \'' . $this->convertToSnakeCase($this->apiNameS) . '\',
            [
        ');
        $count = 0;
        foreach ($this->properties as $property) {
            if (filter_var($property[8], FILTER_VALIDATE_BOOLEAN)) {
                ++$count;
                if ('bool' != $property[1]) {
                    fwrite($persister, '        \'' . $this->convertToSnakeCase($property[0]) . '\' => $' . $this->apiName . '->get' . ucfirst($property[0]) . '()');
                }

                if ('bool' == $property[1]) {
                    fwrite($persister, '        \'' . $this->convertToSnakeCase($property[0]) . '\' => $' . $this->apiName . '->is' . ucfirst($property[0]) . '()');
                }
                if ($count < $this->sizeOfProperties) {
                    fwrite($persister, ',');
                }
            }
        }
        fwrite($persister, '
            ],
            [
                \'uuid\' => $' . $this->apiName . '->getUuid(),
            ],
            [
            ');
        $count = 0;
        foreach ($this->properties as $property) {
            if (filter_var($property[8], FILTER_VALIDATE_BOOLEAN)) {
                ++$count;
                fwrite($persister, '    \'' . $this->convertToSnakeCase($property[0]) . '\' => Types::' . strtoupper($this->convert($property[1], 3)) . '');
                if ($count < $this->sizeOfProperties) {
                    fwrite($persister, ',
            ');
                }
            }
        }
        fwrite($persister, ']
        );

        return $' . $this->apiName . ';
    }

    /**
     * @param ' . ucfirst($this->apiName) . ' $' . $this->apiName . '
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @return ' . ucfirst($this->apiName) . '
     */
    public function delete(' . ucfirst($this->apiName) . ' $' . $this->apiName . '): ' . ucfirst($this->apiName) . '
    {
        $this->connection->delete(
            \'' . $this->convertToSnakeCase($this->apiNameS) . '\',
            [
                \'uuid\' => $' . $this->apiName . '->getUuid(),
            ],
            [
                \'uuid\' => Types::GUID,
            ]
        );

        return $' . $this->apiName . ';
    }
}
');
        fclose($persister);
    }
}
