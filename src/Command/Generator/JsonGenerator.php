<?php

// phpcs:ignoreFile

namespace App\Command\Generator;

use App\Domain\ValueObject\UuidVO;
use Symfony\Component\Console\Command\Command;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class JsonGenerator extends Command
{
    use Generator;

    protected static $defaultName = 'app:generate:json';

    public function startGenerator()
    {
        $this->jsonGenerate();
    }

    protected function configure()
    {
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function jsonGenerate()
    {
        $json = fopen('' . ucfirst($this->apiName) . '.json', 'c+b');
        ftruncate($json, 0);
        fwrite($json, '
{
	"info": {
		"_postman_id": "' . UuidVO::generate() . '",
		"name": " Api' . ucfirst($this->apiName) . '",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "' . ucfirst($this->apiNameS) . '",
			"item": [
				{
					"name": "' . $this->apiName . '_Store",
					"request": {
						"method": "POST",
						"header": [
							{
								"key": "Accept",
								"type": "text",
								"value": "application/json"
							},
							{
								"key": "Content-Type",
								"type": "text",
								"value": "application/json"
							},
							{
								"key": "Authorization",
								"value": "Bearer {{token}}",
								"type": "text"
							},
							{
								"key": "ClientId",
								"value": "{{ClientId}}",
								"type": "text"
							},
							{
								"key": "Accept",
								"value": "application/json;version=1.0",
								"type": "text"
							}
						],
						"url": {
							"raw": "{{base_url}}/' . $this->apiNameS . '?name=zd",
							"host": [
								"{{base_url}}"
							],
							"path": [
								"' . $this->apiNameS . '"
							],
							"query": [
');
        $count = 0;
        foreach ($this->properties as $property) {
            ++$count;
            if (filter_var(filter_var($property[9], FILTER_VALIDATE_BOOLEAN))) {
                fwrite($json, '								
                                        {
                                            "key": "' . $this->convertToSnakeCase($property[0]) . '",
                                            "value": "' . $property[13] . '",
                                            "description": ""
                                        }');
                if ($count <= $this->numberPropertyUpdate) {
                    fwrite($json, ',');
                }
            }
        }

        fwrite($json, '
                                ]
						}
					},
					"response": []
				},
				{
					"name": "' . $this->apiName . '_Show",
					"request": {
						"method": "GET",
						"header": [
							{
								"key": "Authorization",
								"value": "Bearer {{token}}",
								"type": "text"
							},
							{
								"key": "ClientId",
								"value": "{{ClientId}}",
								"type": "text"
							},
							{
								"key": "Accept",
								"value": "application/json;version=1.0",
								"type": "text"
							}
						],
						"url": {
							"raw": "{{base_url}}/' . $this->apiNameS . '/{{' . ucfirst($this->apiName) . 'Id}}",
							"host": [
								"{{base_url}}"
							],
							"path": [
								"' . $this->apiNameS . '",
								"{{' . ucfirst($this->apiName) . 'Id}}"
							]
						}
					},
					"response": []
				},
				{
					"name": "' . $this->apiName . '_Update",
					"request": {
						"method": "PUT",
						"header": [
							{
								"key": "Accept",
								"type": "text",
								"value": "application/json"
							},
							{
								"key": "Content-Type",
								"type": "text",
								"value": "application/json"
							},
							{
								"key": "Authorization",
								"value": "Bearer {{token}}",
								"type": "text"
							},
							{
								"key": "ClientId",
								"value": "{{ClientId}}",
								"type": "text"
							},
							{
								"key": "Accept",
								"value": "application/json;version=1.0",
								"type": "text"
							}
						],
						"url": {
							"raw": "{{base_url}}/' . $this->apiNameS . '/{{' . ucfirst($this->apiName) . 'Id}}?name=UK",
							"host": [
								"{{base_url}}"
							],
							"path": [
								"' . $this->apiNameS . '",
								"{{' . ucfirst($this->apiName) . 'Id}}"
							],
							"query": [
                                ');
        $count = 0;
        foreach ($this->properties as $property) {
            ++$count;
            if (filter_var(filter_var($property[9], FILTER_VALIDATE_BOOLEAN))) {
                fwrite($json, '								
                                        {
                                            "key": "' . $this->convertToSnakeCase($property[0]) . '",
                                            "value": "' . $property[13] . '",
                                            "description": ""
                                        }');
                if ($count <= $this->numberPropertyUpdate) {
                    fwrite($json, ',');
                }
            }
        }

        fwrite($json, '
							]
						}
					},
					"response": []
				},
				{
					"name": "' . $this->apiName . '_Delete",
					"request": {
						"method": "DELETE",
						"header": [
							{
								"key": "Accept",
								"type": "text",
								"value": "application/json"
							},
							{
								"key": "Content-Type",
								"type": "text",
								"value": "application/json"
							},
							{
								"key": "Authorization",
								"value": "Bearer {{token}}",
								"type": "text"
							},
							{
								"key": "ClientId",
								"value": "{{ClientId}}",
								"type": "text"
							},
							{
								"key": "Accept",
								"value": "application/json;version=1.0",
								"type": "text"
							} 
						],
						"url": {
							"raw": "{{base_url}}/' . $this->apiNameS . '/{{' . ucfirst($this->apiName) . 'Id}}?",
							"host": [
								"{{base_url}}"
							],
							"path": [
								"' . $this->apiNameS . '",
								"{{' . ucfirst($this->apiName) . 'Id}}"
							],
							"query": [
								{
									"key": "",
									"value": "",
									"disabled": true
								},
								{
									"key": "",
									"value": "",
									"disabled": true
								},
								{
									"key": "",
									"value": "",
									"disabled": true
								}
							]
						}
					},
					"response": []
				},
				{
					"name": "' . $this->apiName . '_Index",
					"request": {
						"method": "GET",
						"header": [
							{
								"key": "Authorization",
								"type": "text",
								"value": "Bearer {{token}}"
							},
							{
								"key": "ClientId",
								"type": "text",
								"value": "{{ClientId}}"
							},
							{
								"key": "Accept",
								"type": "text",
								"value": "application/json;version=1.0"
							},
							{
								"key": "Content-Type",
								"type": "text",
								"value": "application/json"
							}
						],
						"url": {
							"raw": "{{base_url}}/' . $this->apiNameS . '?q={\n\"name\":\"Beneldux\"\n} ",
							"host": [
								"{{base_url}}"
							],
							"path": [
								"' . $this->apiNameS . '"
							],
							"query": [
								{
									"key": "q",
									"value": "{\n\"name\":\"Beneldux\"\n} "
								}
							]
						}
					},
					"response": []
				}
			],
			"protocolProfileBehavior": {}
		}
	],
	"protocolProfileBehavior": {}
}
        
');

        fclose($json);
    }
}
