<?php

// phpcs:ignoreFile

namespace App\Command\Generator;

use Symfony\Component\Console\Command\Command;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class RepositoryGenerator extends Command
{
    use Generator;

    protected static $defaultName = 'app:generate:repository';

    public function startGenerator()
    {
        $path = $this->projetPath.'src/Domain/Repository/' . ucfirst($this->apiName) . 'RepositoryInterface.php';
        if ('' != $this->folder) {
            if (!file_exists($this->projetPath.'src/Domain/Repository/' . $this->folder)) {
                mkdir($this->projetPath.'src/Domain/Repository/' . $this->folder);
            }
            $path = $this->projetPath.'src/Domain/Repository/' . $this->folder . '/' . ucfirst($this->apiName) . 'RepositoryInterface.php';
        }

        $this->repositoryInterfaceGenerate($path);

        $path = $this->projetPath.'src/Infrastructure/MySQL/Repository/' . ucfirst($this->apiName) . 'Repository.php';
        if ('' != $this->folder) {
            if (!file_exists($this->projetPath.'src/Infrastructure/MySQL/Repository/' . $this->folder)) {
                mkdir($this->projetPath.'src/Infrastructure/MySQL/Repository/' . $this->folder);
            }
            $path = $this->projetPath.'src/Infrastructure/MySQL/Repository/' . $this->folder . '/' . ucfirst($this->apiName) . 'Repository.php';
        }

        $this->repositoryGenerate($path);
    }

    private function repositoryInterfaceGenerate(string $path)
    {
        $repositoryInterface = fopen($path, 'c+b');
        ftruncate($repositoryInterface, 0);
        fwrite($repositoryInterface, '<?php

namespace App\Domain\Repository');
        if ('' != $this->folder) {
            fwrite($repositoryInterface, '\\' . str_replace('/', '\\', $this->folder));
        }
        fwrite($repositoryInterface, ';

use App\Domain\Model\\');
        if ('' != $this->folder) {
            fwrite($repositoryInterface, '' . str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($repositoryInterface, '' . ucfirst($this->apiName) . ';

interface ' . ucfirst($this->apiName) . 'RepositoryInterface
{
    /**
     * Find a ' . $this->convertToEspaceCase($this->apiName) . ' by filters.
     *
     * @param array $filters
     *
     * @return ' . ucfirst($this->apiName) . '|null
     */
    public function find(array $filters): ?' . ucfirst($this->apiName) . ';

    /**
     * List ' . $this->convertToEspaceCase($this->apiNameS) . ' by filters.
     *
     * @param array  $filters
     * @param int    $page
     * @param int    $perPage
     *
     * @return array
     */
    public function list(array $filters = [], int $page = 1, int $perPage = null): array;

    /**
     * Count ' . $this->convertToEspaceCase($this->apiNameS) . ' by filters.
     *
     * @param array  $filters
     *
     * @return int
     */
    public function count(array $filters = []): int;
}
');
        fclose($repositoryInterface);
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function repositoryGenerate(string $path)
    {
        $repository = fopen($path, 'c+b');
        ftruncate($repository, 0);
        fwrite($repository, '<?php

namespace App\Infrastructure\MySQL\Repository');
        if ('' != $this->folder) {
            fwrite($repository, '\\' . str_replace('/', '\\', $this->folder));
        }
        fwrite($repository, ';

use App\Domain\Model\\');
        if ('' != $this->folder) {
            fwrite($repository, '' . str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($repository, '' . ucfirst($this->apiName) . ';
use App\Domain\Repository\\');
        if ('' != $this->folder) {
            fwrite($repository, '' . str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($repository, '' . ucfirst($this->apiName) . 'RepositoryInterface;
use App\Infrastructure\MySQL\Hydrator\Hydrator;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use ReflectionException;

final class ' . ucfirst($this->apiName) . 'Repository extends BaseRepository implements ' . ucfirst($this->apiName) . 'RepositoryInterface
{
    /**
     * ' . ucfirst($this->apiName) . 'Repository constructor.
     *
     * @param Connection $connection
     * @param Hydrator   $hydrator
     */
    public function __construct(
        Connection $connection,
        Hydrator $hydrator
    ) {
        parent::__construct(
            $connection,
            $hydrator,
            ' . ucfirst($this->apiName) . '::class,
            ' . ucfirst($this->apiName) . '::TABLE_NAME,
            ' . ucfirst($this->apiName) . '::TABLE_ALIAS,
            ' . ucfirst($this->apiName) . '::SELECTED_COLUMNS,
            ' . ucfirst($this->apiName) . '::UNIQUE_INDEX
        );
    }

    /**
     * @param array $filters
     *
     * @throws DBALException
     * @throws ReflectionException
     *
     * @return ' . ucfirst($this->apiName) . '|null
     */
    public function find(array $filters): ?' . ucfirst($this->apiName) . '
    {
        return $this->getHydratedResult($filters, $this->selectedColumns);
    }

    /**
     * @param array  $filters
     * @param int    $page
     * @param int    $perPage
     *
     * @throws DBALException
     * @throws ReflectionException
     *
     * @return array
     */
    public function list(array $filters = [], int $page = 1, int $perPage = null): array
    {
        return $this->getHydratedResults($filters, $this->selectedColumns, $page, $perPage);
    }

    /**
     * @param array  $filters
     *
     * @throws DBALException
     * @throws ReflectionException
     *
     * @return int
     */
    public function count(array $filters = []): int
    {
        return $this->countResults($filters);
    }
}
');
        fclose($repository);
    }
}
