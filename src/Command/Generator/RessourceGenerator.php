<?php

// phpcs:ignoreFile

namespace App\Command\Generator;

use Symfony\Component\Console\Command\Command;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class RessourceGenerator extends Command
{
    use Generator;

    protected static $defaultName = 'app:generate:ressource';

    public function startGenerator()
    {
        $path = $this->projetPath.'src/UI/Api/Resource/' . ucfirst($this->apiName) . '.php';
        if ('' != $this->folder) {
            if (!file_exists($this->projetPath.'src/UI/Api/Resource/' . $this->folder)) {
                mkdir($this->projetPath.'src/UI/Api/Resource/' . $this->folder);
            }
            $path = $this->projetPath.'src/UI/Api/Resource/' . $this->folder . '/' . ucfirst($this->apiName) . '.php';
        }
        $this->ressourceGenerate($path);
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function ressourceGenerate(string $path)
    {
        $ressource = fopen($path, 'c+b');
        ftruncate($ressource, 0);
        fwrite($ressource, '<?php

namespace App\UI\Api\Resource');
        if ('' != $this->folder) {
            fwrite($ressource, '\\' . str_replace('/', '\\', $this->folder));
        }
        fwrite($ressource, ';

use App\Domain\Model');
        if ('' != $this->folder) {
            fwrite($ressource, '\\' . str_replace('/', '\\', $this->folder));
        }
        fwrite($ressource, '\\' . ucfirst($this->apiName) . ' as ' . ucfirst($this->apiName) . 'Model;
use Swagger\Annotations as SWG;

/**
 *
');

        if (15 <= $this->sizeOfProperties) {
            fwrite($ressource, ' 
     * @SuppressWarnings(PHPMD.TooManyFields)');
        }
        if (50 <= $this->sizeOfProperties) {
            fwrite($ressource, ' 
    * @SuppressWarnings(ExcessiveMethodLength)');
        }
        fwrite($ressource, '
  */
class ' . ucfirst($this->apiName) . '
{');
        foreach ($this->properties as $property) {
            if (filter_var($property[10], FILTER_VALIDATE_BOOLEAN)) {
                fwrite($ressource, '
    /**
    * ' . ucfirst($this->convertToEspaceCase($property[0])) . '.
    *
    * @SWG\Property(property="' . $this->convertToSnakeCase($property[0]) . '", type="' . $property[1] . '")');
                fwrite($ressource, '
    */
    public $' . $property[0] . ';');
            }
        }

        fwrite($ressource, '

    public function __construct(');

        fwrite($ressource, ucfirst($this->apiName) . 'Model $' . $this->apiName . '');

        fwrite($ressource, ')
    {
');
        foreach ($this->properties as $property) {
            if (filter_var($property[10], FILTER_VALIDATE_BOOLEAN)) {
                fwrite($ressource, '        ');

                fwrite($ressource, '$this->' . $property[0] . ' =');
                if ('bool' != $property[1]) {
                    if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN) && ('datetime' == $property[1] || 'guid' == $property[1])) {
                        fwrite($ressource, ' null !== ');
                    }
                    fwrite($ressource, '$' . $this->apiName . '->get' . ucfirst($property[0]) . '()');
                }

                if ('bool' == $property[1]) {
                    fwrite($ressource, '  $' . $this->apiName . '->is' . ucfirst($property[0]) . '()');
                }
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN) && 'guid' == $property[1]) {
                    fwrite($ressource, '? $' . $this->apiName . '->get' . ucfirst($property[0]));
                }
                if ('guid' == $property[1]) {
                    fwrite($ressource, '->toString()');
                }
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN) && 'datetime' == $property[1]) {
                    fwrite($ressource, '? $' . $this->apiName . '->get' . ucfirst($property[0]) . '()->format(\'Y-m-d\')');
                }

                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN) && ('datetime' == $property[1] || 'guid' == $property[1])) {
                    fwrite($ressource, ' : null');
                }
                fwrite($ressource, ';
');
            }
        }

        fwrite($ressource, '    }

    /**
     * @return array
     */
    public function data(): array
    {
        $data = [
');
        foreach ($this->properties as $property) {
            if (filter_var($property[10], FILTER_VALIDATE_BOOLEAN)) {
                $nameReturn = $this->convertToSnakeCase($property[0]);
                if ($this->convertToSnakeCase($property[0]) == 'uuid') {
                    $nameReturn = 'id';
                }
                fwrite($ressource, '            \'' . $nameReturn . '\' => $this->' . $property[0] . '');
                fwrite($ressource, ',
');
            }
        }
        fwrite($ressource, '        ];
        return $data;
    }
}
');
        fclose($ressource);
    }
}
