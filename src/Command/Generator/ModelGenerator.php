<?php

// phpcs:ignoreFile

namespace App\Command\Generator;

use Symfony\Component\Console\Command\Command;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class ModelGenerator extends Command
{
    use Generator;

    protected static $defaultName = 'app:generate:model';

    public function startGenerator()
    {
        $path = $this->projetPath.'src/Domain/Model/' . ucfirst($this->apiName) . '.php';
        if ('' != $this->folder) {
            if (!file_exists($this->projetPath.'src/Domain/Model/' . $this->folder)) {
                mkdir($this->projetPath.'src/Domain/Model/' . $this->folder);
            }
            $path = $this->projetPath.'src/Domain/Model/' . $this->folder . '/' . ucfirst($this->apiName) . '.php';
        }
    
        $this->ModelGenerate($path);
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function ModelGenerate(string $path)
    {

        $model = fopen($path, 'c+b');
        ftruncate($model, 0);
        fwrite($model, '<?php

namespace App\Domain\Model');
        if ('' != $this->folder) {
            fwrite($model, '\\' . str_replace('/', '\\', $this->folder) . '');
        }
        fwrite($model, ';');

        if ('' != $this->folder) {
            fwrite($model, '

use App\Domain\Model\Model;');
        }

        fwrite($model, '

use App\Domain\ValueObject\UuidVO;

/**
 * Class ' . ucfirst($this->apiName) . '.');

        if (15 <= $this->sizeOfProperties) {
            fwrite($model, ' 
    * @SuppressWarnings(PHPMD.ExcessiveClassLength)
    * @SuppressWarnings(PHPMD.TooManyFields)
    * @SuppressWarnings(PHPMD.ExcessiveParameterList)');
        }

        if (50 < $this->sizeOfProperties) {
            fwrite($model, ' 
    * @SuppressWarnings(ExcessiveMethodLength)');
        }

        fwrite($model, '
 */
class ' . ucfirst($this->apiName) . ' extends Model
{
    public const TABLE_NAME = \' ' . $this->convertToSnakeCase($this->apiNameS) . ' AS ' . $this->tableAlias . '\';
    public const TABLE_ALIAS = \'' . $this->tableAlias . '\';
    public const SELECTED_COLUMNS = <<<EOT
');
        $count = 0;
        foreach ($this->properties as $property) {
            ++$count;
            fwrite($model, '        ' . $this->tableAlias . '.' . $this->convertToSnakeCase($property[0]) . '');
            if ($count < $this->sizeOfProperties) {
                fwrite($model, ',
');
            }
        }
        fwrite($model, '
    EOT;
    public const UNIQUE_INDEX = ');
        foreach ($this->properties as $property) {
            if ($property[5]) {
                fwrite($model, '\''.$property[0].'\'');
                break;
            }
        }

        fwrite($model, ';
    public const NON_ALLOWED_FILTERS = [];
'); //add property into model
        foreach ($this->properties as $property) {
            if ('' != $property[0]) {
                fwrite($model, '
    /**
     * @var ' . $this->convert($property[1], 2) . '
     */
    private $' . $property[0] . ';
');
            }
        }

        fwrite($model, '    /**
     * ' . $this->apiName . ' constructor.
     *');
        foreach ($this->properties as $property) {
            if ('' != $property[0]) {
                fwrite($model, '
     * @param ' . $this->convert($property[1], 2) . '');
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($model, '|null');
                }
                fwrite($model, ' $' . $property[0] . '');
            }
        }
        fwrite($model, '
     */
    public function __construct(
');
        $count = 0;
        foreach ($this->properties as $property) {
            ++$count;
            if ('' != $property[0]) {
                fwrite($model, '        ');
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($model, '?');
                }
                fwrite($model, '' . $this->convert($property[1], 2) . ' $' . $property[0] . '');
                if ($count < $this->sizeOfProperties) {
                    fwrite($model, ',
');
                }
            }
        }
        fwrite($model, '
    ) {
        ');
        foreach ($this->properties as $property) {
            if ('' != $property[0]) {
                fwrite($model, '$this->' . $property[0] . ' = $' . $property[0] . ';
        ');
            }
        }
        fwrite($model, '$this->createdAt = new \DateTime(\'now\');
        $this->updatedAt = new \DateTime(\'now\');
    }

    /**');
        foreach ($this->properties as $property) {
            if ('' != $property[0] && 'uuid' != $property[0]) {
                fwrite($model, '
     * @param ' . $this->convert($property[1], 2) . '');
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($model, '|null');
                }
                fwrite($model, ' $' . $property[0] . '');
            }
        }
        fwrite($model, '
     *
     * @throws \Exception
     *
     * @return ' . ucfirst($this->apiName) . '
     */
    public static function create(
');
        $count = 0;
        foreach ($this->properties as $property) {
            ++$count;
            if ('' != $property[0] && 'uuid' != $property[0]) {
                fwrite($model, '        ');
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($model, '?');
                }
                fwrite($model, '' . $this->convert($property[1], 2) . ' $' . $property[0] . '');
                if ($count < $this->sizeOfProperties) {
                    fwrite($model, ',
');
                }
            }
        }
        fwrite($model, '
    ): self {
        return new self(
');
        $count = 0;
        foreach ($this->properties as $property) {
            ++$count;
            if ('' != $property[0] && 'uuid' != $property[0]) {
                fwrite($model, '            $' . $property[0]);
            }
            if ('uuid' == $property[0]) {
                fwrite($model, '            UuidVO::generate()');
            }
            if ($count < $this->sizeOfProperties) {
                fwrite($model, ',
');
            }
        }
        fwrite($model, '
        );
    }

    /**');
        foreach ($this->properties as $property) {
            if ('' != $property[0] && 'uuid' != $property[0] && filter_var($property[8], FILTER_VALIDATE_BOOLEAN)) {
                fwrite($model, '
     * @param ' . $this->convert($property[1], 2) . '');
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($model, '|null');
                }
                fwrite($model, ' $' . $property[0] . '');
            }
        }
        fwrite($model, '
     *
     * @throws \Exception
     *
     * @return ' . ucfirst($this->apiName) . '
     */
    public function update(
');
        $count = 0;
        foreach ($this->properties as $property) {
            if ($property[8]) {
                ++$count;
                if ('' != $property[0] && 'uuid' != $property[0]) {
                    fwrite($model, '        ');
                    if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                        fwrite($model, '?');
                    }
                    fwrite($model, '' . $this->convert($property[1], 2) . ' $' . $property[0] . '');
                    if ($count < $this->numberPropertyUpdate) {
                        fwrite($model, ',
');
                    }
                }
            }
        }
        fwrite($model, '
    ): self {
');
        foreach ($this->properties as $property) {
            if ($property[8]) {
                if ('' != $property[0] && 'uuid' != $property[0] && filter_var($property[8], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($model, '        $this->set' . ucfirst($property[0]) . '($' . $property[0] . ');
');
                }
            }
        }
        fwrite($model, '        $this->setUpdatedAt(new \DateTime(\'now\'));


        return $this;
    }

');
        foreach ($this->properties as $property) {
            fwrite($model, '
    /**
     * @return ' . $this->convert($property[1], 2) . '');
            if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                fwrite($model, '|null');
            }
            fwrite($model, '
    */
');

            if ('bool' != $property[1]) {
                fwrite($model, '    public function get' . ucfirst($property[0]) . '(): ');
                if ($property[3]) {
                    fwrite($model, '?');
                }
                fwrite($model, $this->convert($property[1], 2) . '');
            }

            if ('bool' == $property[1]) {
                fwrite($model, '    public function is' . ucfirst($property[0]) . '(): ');
                if ($property[3]) {
                    fwrite($model, '?');
                }
                fwrite($model, $this->convert($property[1], 2) . '');
            }

            fwrite($model, '
    {
        return $this->' . $property[0] . ';
    }
');

            if (!filter_var($property[5], FILTER_VALIDATE_BOOLEAN)) {
                fwrite($model, '
    /**
     * @param ' . $this->convert($property[1], 2) . ' $' . $property[0] . '');
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($model, '|null');
                }
                fwrite($model, '
     *
     * @return self
    */
');
                if ('bool' != $property[1]) {
                    fwrite($model, '    public function set' . ucfirst($property[0]) . '(');

                    fwrite($model, '');
                    if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                        fwrite($model, '?');
                    }
                    fwrite($model, '' . $this->convert($property[1], 2) . ' ');
                    fwrite($model, '$' . $property[0] . '): self
');
                }

                if ('bool' == $property[1]) {
                    fwrite($model, '    public function set' . ucfirst($property[0]) . '(');
                    if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                        fwrite($model, '?');
                    }
                    fwrite($model, '' . $this->convert($property[1], 2) . ' ');
                    fwrite($model, '$' . $property[0] . '): self
');
                }

                fwrite($model, '    {
        $this->' . $property[0] . ' = $' . $property[0] . ';

        return $this;
    }
');
            }
        }
        fwrite($model, '}
');
        fclose($model);
    }
}
