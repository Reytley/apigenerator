<?php

// phpcs:ignoreFile

namespace App\Command\Generator;

use Symfony\Component\Console\Command\Command;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class MigrationGenerator extends Command
{
    use Generator;

    protected static $defaultName = 'app:generate:migration';

    public function startGenerator()
    {
        $this->migrationGenerate();
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function migrationGenerate()
    {
        $stringDate = date('YmdHis');

        $ressource = fopen($this->projetPath.'src/Infrastructure/Migrations/Version' . $stringDate . '.php', 'c+b');
        fwrite($ressource, '<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
');
        if (50 < $this->sizeOfProperties) {
            fwrite($ressource, ' 
    * @SuppressWarnings(ExcessiveMethodLength)');
        }

        fwrite($ressource, '
 */
final class Version' . $stringDate . ' extends AbstractMigration
{
    public function getDescription(): string
    {
        return \'Insert Table ' . $this->convertToSnakeCase($this->apiNameS) . '\';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->createTable(\'' . $this->convertToSnakeCase($this->apiNameS) . '\');
    ');
        foreach ($this->properties as $property) {
            if ('' != $property[0]) {
                fwrite($ressource, '
        $table->addColumn(\'' . $this->convertToSnakeCase($property[0]) . '\', \'' . $this->convert($property[1], 1) . '\'');
                if ('' != $property[7]
                    || '' != $property[2]
                    || filter_var($property[3], FILTER_VALIDATE_BOOLEAN)
                    || filter_var($property[4], FILTER_VALIDATE_BOOLEAN)
                ) {
                    fwrite($ressource, ', [');
                }

                if ('' != $property[2]) {
                    fwrite($ressource, '
            \'length\' => \'' . $property[2] . '\',');
                }
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($ressource, '
            \'notnull\' => false,');
                }
                if (filter_var($property[4], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($ressource, '
            \'autoincrement\' => \'true\',');
                }
                if ('' != $property[7]) {
                    fwrite($ressource, '
            \'default\' => \'' . $property[7] . '\',');
                }

                if ('' != $property[7]
                        || '' != $property[2]
                        || filter_var($property[3], FILTER_VALIDATE_BOOLEAN)
                        || filter_var($property[4], FILTER_VALIDATE_BOOLEAN)
                    ) {
                    fwrite($ressource, '
        ]');
                }
                fwrite($ressource, ');');
            }
        }

        foreach ($this->properties as $property) {
            if (filter_var($property[5], FILTER_VALIDATE_BOOLEAN)) {
                fwrite($ressource, '
        $table->setPrimaryKey([\'' . $this->convertToSnakeCase($property[0]) . '\']);');
            }
            if (filter_var($property[6], FILTER_VALIDATE_BOOLEAN)) {
                fwrite($ressource, '
        $table->addUniqueIndex([\'' . $this->convertToSnakeCase($property[0]) . '\']);');
            }
        }

        fwrite($ressource, '
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable(\'' . $this->convertToSnakeCase($this->apiNameS) . '\');
    }
}
');
    }
}
