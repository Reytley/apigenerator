<?php

// phpcs:ignoreFile

namespace App\Command\Generator;

use Symfony\Component\Console\Command\Command;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class InputGenerator extends Command
{
    use Generator;

    protected static $defaultName = 'app:generate:input';

    public function startGenerator()
    {
        $path = $this->projetPath.'src/UI/Api/Input/' . ucfirst($this->apiName) . '/Input.php';
        if ('' != $this->folder) {
            if (!file_exists( $this->projetPath.'src/UI/Api/Input/' . $this->folder . '/' . ucfirst($this->apiName))) {
                print_r( $this->projetPath.'src/UI/Api/Input/' . $this->folder . '/' . ucfirst($this->apiName));
                if (!file_exists( $this->projetPath.'src/UI/Api/Input/' . $this->folder)) {
                    mkdir( $this->projetPath.'src/UI/Api/Input/' . $this->folder);
                }
                mkdir( $this->projetPath.'src/UI/Api/Input/' . $this->folder . '/' . ucfirst($this->apiName));
            }
            $path = ' $this->projetPath.src/UI/Api/Input/' . $this->folder . '/' . ucfirst($this->apiName) . '/Input.php';
        }
        if ('' == $this->folder) {
            if (!file_exists( $this->projetPath.'src/UI/Api/Input/' . ucfirst($this->apiName) . '')) {
                mkdir( $this->projetPath.'src/UI/Api/Input/' . ucfirst($this->apiName));
            }
        }

        $this->inputGenerate($path);
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function inputGenerate(string $path)
    {
        $input = fopen($path, 'c+b');
        ftruncate($input, 0);
        fwrite($input, '<?php

namespace App\UI\Api\Input\\');
        if ('' != $this->folder) {
            fwrite($input, str_replace('/', '\\', $this->folder) . '\\');
        }
        fwrite($input, '' . ucfirst($this->apiName) . ';

');

        foreach ($this->properties as $property) {
            if (filter_var('guid' == $property[1] && filter_var($property[9], FILTER_VALIDATE_BOOLEAN))) {
                fwrite($input, 'use App\Domain\ValueObject\UuidVO;');
                break;
            }
        }

        fwrite($input, 'use App\UI\Api\Input\InputInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @SWG\Definition(title="' . ucfirst($this->apiName) . '/Store", definition="' . ucfirst($this->apiName) . '/Store")
');

        if (15 <= $this->sizeOfProperties) {
            fwrite($input, ' 
     * @SuppressWarnings(PHPMD.ExcessiveClassLength)
     * @SuppressWarnings(PHPMD.TooManyFields)');
        }
        fwrite($input, '
  */
class Input implements InputInterface
{');
        foreach ($this->properties as $property) {
            if (filter_var($property[9], FILTER_VALIDATE_BOOLEAN)) {
                fwrite($input, '
    /**
     * ' . $property[0] . '.
     *
     * @SWG\Property(property="' . $this->convertToSnakeCase($property[0]) . '", type="' . $this->convert($property[1], 5) . '", format="' . $this->convert($property[1], 3) . '")');
                if (!filter_var($property[3], FILTER_VALIDATE_BOOLEAN) && 'bool' != $property[1]) {
                    fwrite($input, '
     * @Assert\NotBlank()');
                }

                if ('guid' == $property[1]) {
                    fwrite($input, '
     * @Assert\Uuid()');
                }
                if ('guid' != $property[1]) {
                    fwrite($input, '
     * @Assert\Type(type="' . $this->convert($property[1], 6) . '")');
                }
                fwrite($input, '
     */
    private $' . $property[0] . ';');
            }
        }

        foreach ($this->properties as $property) {
            if (filter_var($property[9], FILTER_VALIDATE_BOOLEAN)) {
                fwrite($input, '
    /**
     * @return ' . $this->convert($property[1], 2) . '');
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($input, '|null');
                }
                fwrite($input, '
    */
');

                if ('bool' != $property[1]) {
                    fwrite($input, '    public function get' . ucfirst($property[0]) . '(): ');
                    if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                        fwrite($input, '?');
                    }
                    fwrite($input, $this->convert($property[1], 5));
                }

                if ('bool' == $property[1]) {
                    fwrite($input, '
    public function is' . ucfirst($property[0]) . '(): ');
                    if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                        fwrite($input, '?');
                    }
                    fwrite($input, $this->convert($property[1], 5));
                }

                fwrite($input, '
    {
        return $this->' . $property[0] . ';
    }
');
                fwrite($input, '
    /**
     * @param ' . $this->convert($property[1], 5) . '');
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($input, '|null');
                }
                fwrite($input, ' $' . $property[0] . '');
                fwrite($input, '
     *
     * @return self
     */
');

                fwrite($input, '    public function set' . ucfirst($property[0]) . '(');
                if (filter_var($property[3], FILTER_VALIDATE_BOOLEAN)) {
                    fwrite($input, '?');
                }
                fwrite($input, '' . $this->convert($property[1], 5) . ' ');
                fwrite($input, '$' . $property[0] . '): self
');

                fwrite($input, '    {
        $this->' . $property[0] . ' =');
                if ('bool' != $property[1]) {
                    fwrite($input, ' !empty($' . $property[0] . ') ? ');
                }
                if ('bool' != $property[1] && 'guid' != $property[1] && 'datetime' != $property[1]) {
                    fwrite($input, '$' . $property[0] . '');
                }
                if ('guid' == $property[1]) {
                    fwrite($input, ' UuidVO::fromString($' . $property[0] . ')');
                }
                if ('bool' == $property[1]) {
                    fwrite($input, ' filter_var($' . $property[0] . ', FILTER_VALIDATE_BOOLEAN) ');
                }
                if ('datetime' == $property[1]) {
                    fwrite($input, ' new \DateTime($' . $property[0] . ')');
                }
                if ('bool' != $property[1]) {
                    fwrite($input, ' : null');
                }

                fwrite($input, ';

        return $this;
    }
');
            }
        }
        fwrite($input, '}
');
        fclose($input);
    }
}
