<?php

// phpcs:ignoreFile

namespace App\Command\Generator;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
trait Generator
{
    private $projetPath;
    private $apiName;
    private $apiNameS;
    private $properties;
    private $folderType;
    private $folder;
    private $sizeOfProperties;
    private $tableAlias;
    private $numberPropertyUpdate;
    private $numberPropertyInput;
    private $role;

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fic = fopen($_ENV['APHRODITE_FOLDER'].'src/Command/data/data.csv', 'rb');
        $this->projetPath = $_ENV['APHRODITE_FOLDER'];
        $ligne = 0;
        $propertyNumber = 1;
        while ($tab = fgetcsv($fic, 2048, ';')) {
            ++$ligne;
            if (3 == $ligne) {
                $this->apiName = $tab[0];

                if ('' != $tab[1]) {
                    $this->folder = $tab[1];
                    $this->folderType = '/' . $tab[1];
                }
            }
            if (5 == $ligne) {
                $this->apiNameS = $tab[0];
                if ('' != $tab[1]) {
                    $this->role = $tab[1];
                }
            }
            if ($ligne > 6) {
                $this->properties[$propertyNumber] = $tab;
                ++$propertyNumber;
            }
        }

        $count = 0;
        foreach ($this->properties as $property) {
            if (filter_var($property[8], FILTER_VALIDATE_BOOLEAN)) {
                ++$count;
            }
        }
        $this->numberPropertyUpdate = $count;

        $count = 0;
        foreach ($this->properties as $property) {
            if (filter_var($property[9], FILTER_VALIDATE_BOOLEAN)) {
                ++$count;
            }
        }

        $this->numberPropertyInput = $count;

        $this->tableAlias = substr(strtolower($this->apiNameS), 0, 2);
        $this->tableAlias .= substr(strtolower($this->apiNameS), -2, 2);


        $this->sizeOfProperties = \count($this->properties);

        $this->startGenerator();

        return 1;
    }

    private function convertToSnakeCase($nameOfproperty)
    {
        return strtolower(ltrim(preg_replace(
            '/[A-Z]([A-Z](?![a-z]))*/',
            '_$0',
            $nameOfproperty
        ), '_'));
    }

    private function convertToEspaceCase($nameOfproperty)
    {
        return strtolower(ltrim(preg_replace(
            '/[A-Z]([A-Z](?![a-z]))*/',
            ' $0',
            $nameOfproperty
        ), ' '));
    }

    private function convertToPointCase($nameOfproperty)
    {
        return strtolower(ltrim(preg_replace(
            '/[A-Z]([A-Z](?![a-z]))*/',
            '.$0',
            $nameOfproperty
        ), '.'));
    }

    private function convert($type, $class)
    {
        //For Migration
        if (1 == $class) {
            switch ($type) {
                case 'int':
                    return 'integer';
                case 'uuid':
                    return 'guid';
                    case 'bool':
                        return 'boolean';
                default:
                    return $type;
            }
        }

        //For Model
        if (2 == $class) {
            switch ($type) {
                case 'guid':
                    return 'UuidVO';
                case 'datetime':
                    return '\DateTime';
                default:
                    return $type;
            }
        }

        //For persister
        if (3 == $class) {
            switch ($type) {
                case 'int':
                    return 'integer';
                case 'uuid':
                    return 'guid';
                case 'bool':
                    return 'boolean';
                case 'dateTime':
                    return 'DATETIME_MUTABLE';
                default:
                    return $type;
            }
        }

        if (4 == $class) {
            switch ($type) {
                case 'bool':
                    return 'boolean';
                default:
                    return $type;
            }
        }

        //For Input
        if (5 == $class) {
            switch ($type) {
                default:
                    return 'string';
            }
        }
        //For Input
        if (6 == $class) {
            switch ($type) {
                case 'int':
                    return 'string';
                case 'float':
                    return 'string';
                case 'datetime':
                    return '\DateTime';
                default:
                    return $type;
            }
        }
    }
}
