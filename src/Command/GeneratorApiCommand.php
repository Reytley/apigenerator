<?php

// phpcs:ignoreFile

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class GeneratorApiCommand extends Command
{
    protected static $defaultName = 'app:api-generate';

    protected function configure()
    {
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo exec('php bin/console app:generate:input');
        $this->stepEcho('Generate input');
        echo exec('php bin/console app:generate:migration');
        $this->stepEcho('Generate migration');
        echo exec('php bin/console app:generate:model');
        $this->stepEcho('Generate model');
        echo exec('php bin/console app:generate:persister');
        $this->stepEcho('Generate persister');
        echo exec('php bin/console app:generate:repository');
        $this->stepEcho('Generate repository');
        echo exec('php bin/console app:generate:ressource');
        $this->stepEcho('Generate ressource');
        echo exec('php bin/console app:generate:service');
        $this->stepEcho('Generate service');
        echo exec('php bin/console app:generate:controller');
        $this->stepEcho('Generate controller');
    }

    private function stepEcho(string $message){
        echo <<<END
        ==========================================================
        $message Done !
        ==========================================================

        END;
    }
}
