#!/bin/bash

PROJECT_NAME = apigenerator
DOCKER_PHP_CONTAINER_NAME = $(PROJECT_NAME)_php-fpm

UID = $(shell id -u)
GID = $(shell id -g)
WWW_USER = $(UID):$(GID)

STEP = "\\n\\r==========================================================\\n"

base:
	@echo "$(STEP) Starting Docker $(STEP)";
	@if ! pgrep -f docker > /dev/null; then if [ -f /etc/init.d/docker ]; then /etc/init.d/docker reload; else open -a Docker; fi fi
	@until docker ps > /dev/null 2>&1; do sleep 1; done;
	@echo "$(STEP) Copying parameter files $(STEP)";
	@if [ ! -f .env ]; then cp .env.docker .env; fi

build: base ## Builds the Docker images required to run this project
	@echo "$(STEP) Building images $(STEP)";
	@docker-compose -f docker-compose.yml build;
	@echo "$(STEP) Done ! $(STEP)";

build-update: base ## Same as build option, but updates images before build
	@echo "$(STEP) Pulling images $(STEP)";
	@docker-compose -f docker-compose.yml pull;
	@echo "$(STEP) Create network $(STEP)";
	@docker network inspect $(PROJECT_NAME)_default --format {{.Id}} 2>/dev/null || docker network create --driver bridge $(PROJECT_NAME)_default;
	@echo "$(STEP) Building images $(STEP)";
	@docker-compose -f docker-compose.yml build --pull;
	@echo "$(STEP) Done ! $(STEP)";

do-up:
	@echo "$(STEP) Starting up project containers $(STEP)";
	@docker-compose -f docker-compose.yml up -d;
	@docker exec -it $(DOCKER_PHP_CONTAINER_NAME) composer self-update;
	@docker exec -it -u $(WWW_USER) $(DOCKER_PHP_CONTAINER_NAME) composer --version;
	@echo "$(STEP) Done ! $(STEP)";

install: build-update do-up ## Installs the project (to run the first time you want to launch the project)
	@if [ ! -f composer.lock ]; then echo "$(STEP) Updating dependencies $(STEP)" && docker exec -it -u $(WWW_USER) $(DOCKER_PHP_CONTAINER_NAME) composer update --prefer-dist; else echo "$(STEP) Installing dependencies $(STEP)" && docker exec -it -u $(WWW_USER) $(DOCKER_PHP_CONTAINER_NAME) composer install --prefer-dist; fi
	@docker exec -it -u $(WWW_USER) $(DOCKER_PHP_CONTAINER_NAME) composer install --prefer-dist;
	@echo "$(STEP) Configuring development environment variables $(STEP)";
	@docker exec -it -u $(WWW_USER) $(DOCKER_PHP_CONTAINER_NAME) composer dump-env dev;
	@echo "$(STEP) Project installation finished ! $(STEP)";

up: build do-up ## Starts the project and containers

stop: ## Stops the project and containers
	@echo "$(STEP) Stopping containers $(STEP)";
	@docker-compose -f docker-compose.yml stop;
	@echo "$(STEP) Done ! $(STEP)";

remove: ## Stops and removes containers (may cause data loss)
	@echo "$(STEP) Stopping and removing containers $(STEP)";
	@docker-compose -f docker-compose.yml down --remove-orphans;
	@echo "$(STEP) Done ! $(STEP)";

generate-api: ## Enter in PHP container CLI
	@echo "$(STEP) Generate API $(STEP)";
	@docker start $(DOCKER_PHP_CONTAINER_NAME);
	@docker exec -it -u $(WWW_USER) $(DOCKER_PHP_CONTAINER_NAME) php bin/console app:api-generate;
	@docker stop $(DOCKER_PHP_CONTAINER_NAME);
	@echo "$(STEP) Done ! $(STEP)";