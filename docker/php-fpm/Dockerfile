# docker/php-fpm/Dockerfile

# PHP version
ARG PHP_VERSION

# PHP stage
FROM php:${PHP_VERSION}-fpm

# Get arguments
ARG PHP_FPM_PATH_FOLDER
ARG PHP_FPM_PATH_CONF_FOLDER

# Update and install
RUN apt-get update && \
	apt-get install -y libcurl3-dev \
		git \
		libzip-dev \
		unzip \
		wget \
		libxslt1-dev \
		libicu-dev \
		libgd-dev \
		zip

RUN docker-php-ext-install gd pdo_mysql zip xsl intl soap exif bcmath

# Install xdebug
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && echo "#zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.start_with_request=no" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.idekey=idekey" >> /usr/local/etc/php/conf.d/xdebug.ini

# Install Composer latest
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
	php -r "copy('https://composer.github.io/installer.sig', 'installer.sig');" && \
	php -r "if (hash_file('SHA384', 'composer-setup.php') === trim(file_get_contents('installer.sig'))) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); exit(1); } echo PHP_EOL;" && \
	php composer-setup.php --install-dir=/usr/bin --filename=composer --quiet && \
	php -r "unlink('composer-setup.php');"
RUN composer --version
RUN composer self-update 1.10.23

# Override php.ini settings
COPY ${PHP_FPM_PATH_CONF_FOLDER}php.ini /usr/local/etc/php/php.ini
RUN php --version

RUN useradd -ms /bin/bash -u 1337 user

WORKDIR /var/www

COPY ${PHP_FPM_PATH_FOLDER}docker-entrypoint.sh /entrypoint.sh

RUN chmod u+x /entrypoint.sh && \
    mkdir /entrypoint-init.d

CMD ["/entrypoint.sh"]
